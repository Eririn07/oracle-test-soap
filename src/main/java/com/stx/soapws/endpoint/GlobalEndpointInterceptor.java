package com.stx.soapws.endpoint;

import jakarta.xml.soap.*;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.interceptor.EndpointInterceptorAdapter;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import javax.xml.namespace.QName;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Component
public class GlobalEndpointInterceptor implements EndpointInterceptor {

    @Override
    public boolean handleRequest(MessageContext messageContext, Object o) throws Exception {
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object o) throws Exception {
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object o) throws Exception {
        return false;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object o, Exception e) throws Exception {
        try {
            SOAPMessage soapMessage = ((SaajSoapMessage) messageContext.getResponse()).getSaajMessage();
            SOAPBody body = soapMessage.getSOAPBody();
            Iterator<Node> it = body.getChildElements();
            while (it.hasNext()) {
                SOAPBodyElement node = (SOAPBodyElement) it.next();
                node.removeAttribute("xmlns:ns2");
                node.addAttribute(QName.valueOf("xmlns:xsi"), "http://www.w3.org/2001/XMLSchema-instance");
                node.setPrefix("tns");
                Iterator<Node> it2 = node.getChildElements();
                while (it2.hasNext()) {
                    SOAPBodyElement node2 = (SOAPBodyElement) it2.next();
                    node2.setPrefix("tns");
                }
            }
        } catch (SOAPException exx) {
            exx.printStackTrace();
        }
    }
}