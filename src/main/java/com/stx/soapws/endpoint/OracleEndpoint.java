package com.stx.soapws.endpoint;

import com.stx.soapws.model.*;
import com.stx.soapws.service.OracleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@Slf4j
public class OracleEndpoint {

    public final OracleService oracleService;

    public static final String NAMESPACE_URI = "http://www.oracle.com/external/services/sampleservice/request/v1.0";

    public OracleEndpoint(OracleService oracleService) {
        this.oracleService = oracleService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "sampleservicerq")
    @ResponsePayload
    public GetOracleResponse getOracle(@RequestPayload GetOracleRequest request) {
        GetOracleResponse response = new GetOracleResponse();

        sampleservicerq samplerq = new sampleservicerq();

        samplerq.setType(request.getType());
        samplerq.setOrder_type(request.getOrderType());
        samplerq.setService_id(request.getServiceId());
        samplerq.setTrx_id(request.getTrxId());

        OracleRequestDTO requestDTO = new OracleRequestDTO();

        requestDTO.setSampleservicerq(samplerq);

        OracleResponseDTO data = oracleService.getDataFromEndpoint(requestDTO);

        response.setErrorCode(data.getSampleservicers().getError_code());
        response.setErrorMsg(data.getSampleservicers().getError_msg());
        response.setTrxId(request.getTrxId());

        return response;
    }
}
