@jakarta.xml.bind.annotation.XmlSchema(
        namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0",
        elementFormDefault = jakarta.xml.bind.annotation.XmlNsForm.QUALIFIED)
        // include the remaining namespace declarations

package com.stx.soapws.endpoint;

import jakarta.xml.bind.annotation.XmlNs;