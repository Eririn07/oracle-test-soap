package com.stx.soapws.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OracleRequestDTO {

    public sampleservicerq sampleservicerq;
}
