package com.stx.soapws.service;

import com.stx.soapws.model.OracleRequestDTO;
import com.stx.soapws.model.OracleResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class OracleService {

    @Value("${oracle.rest.sample-service.url}")
    String oracleServiceEndpointUrl;

    public OracleResponseDTO getDataFromEndpoint(OracleRequestDTO request) {
        log.info("POST data to: {} with request: {}", oracleServiceEndpointUrl, request.toString());
        OracleResponseDTO responseDTOResponseEntity = null;

        ResponseEntity<OracleResponseDTO> responseEntity = null;
                
        try {

            RestTemplate rest = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();

            HttpEntity<OracleRequestDTO> requestEntity = new HttpEntity<>(request, headers);

            responseEntity = rest.exchange(
                    oracleServiceEndpointUrl,
                    HttpMethod.POST,
                    requestEntity,
                    OracleResponseDTO.class
            );

        } catch (Exception E) {
            log.error(E.getMessage());
        }

        if(responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()){
            return responseEntity.getBody();
        }else{
            throw new RuntimeException("failed to get from POST");
        }

    }
}
