package com.stx.soapws.config;

import com.stx.soapws.endpoint.GlobalEndpointInterceptor;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.util.List;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/external/services/ws/*");
    }

    @Bean(name = "oracle")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema oracleSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("oraclePort");
        wsdl11Definition.setLocationUri("/external/services/ws/sample-service");
        wsdl11Definition.setTargetNamespace("http://www.oracle.com/external/services/sampleservice/response/v1.0");
        wsdl11Definition.setSchema(oracleSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema oracleSchema() {
        return new SimpleXsdSchema(new ClassPathResource("oracle.xsd"));
    }

    @Override
    public void addInterceptors(List<EndpointInterceptor> interceptors) {
        interceptors.add(new GlobalEndpointInterceptor());
    }
}
